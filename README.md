# cogment/grpc_cpp_buildenv

This is a simple docker image that is apropriate for compiling most gRPC projects,
including CI-related tasks for the the following projects:

* https://gitlab.com/cogment/variadic_future
* https://gitlab.com/cogment/easy_grpc

It can also make a decent **build** environment for projects making use gRPC and the
above projects, but may be too heavy for use as the base for a production environment.
