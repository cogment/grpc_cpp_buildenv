FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=America/New_York

RUN apt-get update && apt-get install -y \
  build-essential \
  cmake \
  git \
  clang \
  autogen \
  autoconf \
  libtool \
  libgflags-dev \
  clang-format \
  clang-tidy \
  zlib1g-dev \
  valgrind \
  vim \
  && rm -rf /var/lib/apt/lists/*

# GRPC
ARG GRPC_RELEASE_TAG=v1.41.0
ARG GRPC_BUILD_TYPE=Release
RUN git clone -b ${GRPC_RELEASE_TAG} --single-branch https://github.com/grpc/grpc /var/local/src/grpc && \
    cd /var/local/src/grpc && \
    git submodule update --init --recursive
RUN cd /var/local/src/grpc && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=${GRPC_BUILD_TYPE} -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF .. && \
    make CXXFLAGS="-g" CFLAGS="-g" -j$(nproc) install
RUN cd /var/local/src/grpc/third_party/abseil-cpp && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=${GRPC_BUILD_TYPE} -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE .. && \
    make CXXFLAGS="-g" CFLAGS="-g" -j$(nproc) install
RUN cd / && \
    rm -rf /var/local/src/grpc

# Setting up the C++ and Python gRPC plugins
RUN ln -s /usr/local/bin/grpc_cpp_plugin /usr/local/bin/protoc-gen-grpc_cpp && \
    ln -s /usr/local/bin/grpc_python_plugin /usr/local/bin/protoc-gen-grpc_python

# Gtest
ARG GOOGLETEST_RELEASE_TAG=v1.10.x
RUN git clone -b ${GOOGLETEST_RELEASE_TAG} --single-branch https://github.com/google/googletest.git /var/local/src/gtest && \
    cd /var/local/src/gtest && \
    mkdir _bld && \
    cd _bld && \
    cmake .. && \
    make -j$(nproc) install && \
    cd / && \
    rm -rf /var/local/src/gtest

